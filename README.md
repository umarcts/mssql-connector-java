# Microsoft SQL Server Java Connector

This SQL connector for Java allows [Apache Sqoop](http://sqoop.apache.org) to
ingest data from Microsoft SQL Server into Hive.

## Building
The gzipped tarball for version 4.2.6420.100 must be downloaded from
[Microsoft](https://msdn.microsoft.com/en-us/sqlserver/aa937724.aspx) and
placed in the SOURCES directory before building.
