# Note: although this is a noarch package, it will only build on x86 or x86_64
# hosts, because you need java-1.6.0-openjdk to build it and that is only
# provided in RHEL6 for those architectures.  (The resulting noarch package
# should work everywhere, though.)  To avoid having to retry builds in brew
# multiple times until you get the right build host, use
#	rhpkg build --target rhel-6.3-noarch-candidate
# (adjust branch number as appropriate).


%global     builddir        build-mssql-jdbc
%global     distdir         dist-mssql-jdbc
%global     java6_rtpath    %{java_home}/jre/lib/rt.jar
%global     java6_javacpath /usr/bin/javac
%global     java6_javapath  /usr/bin/javac

Summary:    Official JDBC driver for Microsoft SQL Server
Name:       mssql-connector-java
Version:    4.2.6420
Release:    6%{?dist}
Epoch:      1 
License:    Microsoft JDBC License
Group:      System Environment/Libraries
URL:        https://msdn.microsoft.com/en-us/sqlserver/aa937724.aspx

# NOTE: This needs to be downloaded in advance and placed in SOURCES/
Source0:            sqljdbc_4.2.6420.100_enu.tar.gz

BuildRoot:          %{_tmppath}/%{name}-%{epoch}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:          noarch

Requires: java
Requires: jpackage-utils
Requires(post): jpackage-utils
Requires(postun): jpackage-utils

%description
Microsoft's JDBC driver. Added for sqoop.

%prep
%setup -q -n sqljdbc_4.2

%build

%install
rm -rf $RPM_BUILD_ROOT

install -d -m 755 $RPM_BUILD_ROOT%{_javadir}
install -m 644 $RPM_BUILD_DIR/sqljdbc_4.2/enu/sqljdbc42.jar \
    $RPM_BUILD_ROOT%{_javadir}/%{name}-%{version}.jar

(cd $RPM_BUILD_ROOT%{_javadir} && for jar in *-%{version}*.jar; do ln -sf ${jar} `echo $jar| sed  "s|-%{version}||g"`; done)

%post
%update_maven_depmap

%postun
%update_maven_depmap

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(0644,root,root,0755)
%attr(0644,root,root) %{_javadir}/*.jar
